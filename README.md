A naive implementation:  

```
   \ ¦ /  
    \¦/  
  ---*---  
    /¦\  
   / ¦ \  
```

The picture above shows a node with 8 links with uniform stiffness and damping coefficient.

Force on each node contributed by the the 8 links is computed, then the result is used to update the position of the node.

Further details can be found at:
https://docs.google.com/open?id=0B8bH3tisoTPJZkdzMmlLZ1M1TmM 

MATLAB Central link: http://www.mathworks.ch/matlabcentral/fileexchange/37907-deformable-object-with-naive-mass-spring-damper-model